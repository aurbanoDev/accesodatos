package com.aurbano.musicManagement.app;

import java.io.File;

/**
 * Conjunto de constantes utilizadas en la aplicacion
 * @author Adrian Urbano
 */
public class Constants {
    /**
     * Indexado del JTabbedPane
     */
    public static final int ALBUM_TAB = 0;
    public static final int SONG_TAB = 1;
    public static final int CLIP_TAB = 2;

    public static final String PROPERTIES_PATH = System.getProperty("user.home") + File.separator + "config.prop";
    public static final String MYSQL = "MySQL";
    public static final String POSTGRESQL = "PostGreSQL";

    /**
     * Cabeceras
     */
    public static final String[] ALBUM_HEADER =
            {"ID", "NAME", "R. INDUSTRY", "BAND NAME", "UNITS SOLD", "P. DATE"};
    public static final String[] CLIP_HEADER =
            {"ID", "COST", "SHOOTING DAYS", "DIRECTOR NAME", "STUDIO NAME", "RECORDING DATE"};
    public static final String[] SONG_HEADER =
            {"ID", "NAME", "MUSIC COMPOSER", "LYRICS COMPOSER", "DURATION", "RELEASE DATE"};

    /**
     * Tablas
     */
    public static final String ID = "id";

    public static final String ALBUM_TABLE = "albums";
    public static final String ALBUM_NAME = "name";
    public static final String RECORD_INDUSTRY = "recordIndustry";
    public static final String BAND_NAME = "bandName";
    public static final String UNITS_SOLD = "unitsSold";
    public static final String PRESENTATION_DATE = "presentationDate";


    public static final String SONG_TABLE = "songs";
    public static final String SONG_NAME = "name";
    public static final String MUSIC_COMPOSER = "musicComposer";
    public static final String LYRICS_COMPOSER = "lyricsComposer";
    public static final String DURATION = "duration";
    public static final String RELEASE_DATE = "releaseDate";
    public static final String ID_ALBUM = "id_album";
    public static final String ID_VIDEOCLIP = "id_videoclip";


    public static final String CLIP_TABLE = "videoclips";
    public static final String COST = "cost";
    public static final String SHOOTING_DAYS = "shootingDays";
    public static final String DIRECTOR_NAME = "directorName";
    public static final String STUDIO_NAME = "studioName";
    public static final String RECORDING_DATE = "recordigDate";
    public static final String ID_SONG = "id_song";

}
