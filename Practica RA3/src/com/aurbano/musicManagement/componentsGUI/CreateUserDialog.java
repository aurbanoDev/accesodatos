package com.aurbano.musicManagement.componentsGUI;

import com.aurbano.musicManagement.componentsGUI.combos.MyComboBox;
import com.aurbano.musicManagement.music.Group;
import com.aurbano.musicManagement.music.User;
import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;

public class CreateUserDialog extends JDialog {
    private JPanel contentPane;
    private JTabbedPane tabbedPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private MyComboBox cbGroups;
    private JList<Group> jlGroups;
    private JTextField tfUsername;
    private JTextField tfPassword;
    private JPanel userPanel;
    private JPanel groupPanel;
    private JTextField tfGroupName;
    private JDateChooser creationDateChooser;
    private MyComboBox cbUsers;
    private JList<User> jlUsers;
    private JList<User> userListUserTab;
    private JList<Group> groupListGroupTab;
    private JButton deleteButton;
    private JButton newButton;
    private JButton editButton;

    private DefaultListModel<Group> groupsListModel;
    private DefaultListModel<User> usersListModel;

    private boolean editMode;

    public CreateUserDialog() {
        groupsListModel = new DefaultListModel<>();
        usersListModel = new DefaultListModel<>();
        jlUsers.setModel(usersListModel);
        jlGroups.setModel(groupsListModel);
        userListUserTab.setModel(usersListModel);
        groupListGroupTab.setModel(groupsListModel);
        setAdminMode(false);

        setContentPane(contentPane);
        setModal(true);
        pack();
        setLocationRelativeTo(null);
    }

    public void setListeners(ActionListener listener) {
        buttonOK.addActionListener(listener);
        buttonCancel.addActionListener(listener);
        jlUsers.addListSelectionListener((ListSelectionListener) listener);
        jlGroups.addListSelectionListener((ListSelectionListener) listener);
        userListUserTab.addListSelectionListener((ListSelectionListener) listener);
        groupListGroupTab.addListSelectionListener((ListSelectionListener) listener);
        deleteButton.addActionListener(listener);
        editButton.addActionListener(listener);
        newButton.addActionListener(listener);
    }

    public JList<Group> getJlGroups() {
        return jlGroups;
    }

    public JList<User> getJlUsers() {
        return jlUsers;
    }


    public JList<User> getUserListUserTab() {
        return userListUserTab;
    }

    public JList<Group> getGroupListGroupTab() {
        return groupListGroupTab;
    }

    public void refreshGroupList(List<Group> groupList) {
        groupsListModel.removeAllElements();

        for (Group group : groupList)
            groupsListModel.addElement(group);
    }

    public void refreshUserList(List<User> usersList) {
        usersListModel.removeAllElements();

        for (User user : usersList)
            usersListModel.addElement(user);
    }

    public String getUsername() {
        return tfUsername.getText();
    }

    public String getPassword() {
        return tfPassword.getText();
    }

    public String getGroupName() {
        return tfGroupName.getText();
    }

    public User getSelectedUser() {
        return userListUserTab.getSelectedValue();
    }

    public Group getSelectedGroup() {
        return groupListGroupTab.getSelectedValue();
    }

    public int getCurrentIndex() {
        return tabbedPane.getSelectedIndex();
    }

    public boolean isEditMode() {
        return editMode;
    }

    public void setEditMode(boolean editMode) {
        this.editMode = editMode;
    }

    public void clearInputfields() {
        tfUsername.setText("");
        tfPassword.setText("");
        tfGroupName.setText("");
        creationDateChooser.setDate(null);
    }

    public Date getCreationDate() {
        return creationDateChooser.getDate();
    }

    public void loadUserData(User user) {
        if (user == null)
            return;

        tfUsername.setText(user.getUsername());
        tfPassword.setText(user.getPassword());

        if (user.getGroups() != null)
            cbGroups.refreshList(user.getGroups());
    }

    public void loadGroupData(Group group) {
        if (group == null)
            return;

        tfGroupName.setText(group.getName());
        creationDateChooser.setDate(group.getCreationDate());

        if (group.getUsers() != null)
            cbUsers.refreshList(group.getUsers());
    }

    public void setViewMode() {
        setEditableMode(false);
        clearInputfields();
        userListUserTab.clearSelection();
        groupListGroupTab.clearSelection();
    }

    public void setEditableMode(boolean editable) {
        cbUsers.clearSelection();
        cbGroups.clearSelection();

        tfUsername.setEditable(editable);
        tfPassword.setEditable(editable);
        tfGroupName.setEditable(editable);
        jlUsers.setEnabled(editable);
        jlGroups.setEnabled(editable);
        creationDateChooser.setEnabled(editable);
        buttonOK.setEnabled(editable);

        userListUserTab.setEnabled(!editable);
        groupListGroupTab.setEnabled(!editable);
        cbGroups.setEnabled(!editable);
        cbUsers.setEnabled(!editable);
    }

    public void setAdminMode(boolean admin) {
        deleteButton.setEnabled(admin);
        editButton.setEnabled(admin);
        setEditableMode(!admin);
    }
}
