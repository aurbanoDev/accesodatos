package com.aurbano.musicManagement.componentsGUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Conjunto de botones.
 * @author Adrian Urbano
 */
public class ButtonsPanel extends JPanel {

    private JButton save;
    private JButton edit;
    private JButton delete;
    private JButton cancel;

    public ButtonsPanel() {
        setLayout(new FlowLayout());

        save = new JButton(" Save ");
        save.setActionCommand("save");
        edit = new JButton(" Edit  ");
        edit.setActionCommand("edit");
        delete = new JButton("Delete");
        delete.setActionCommand("delete");
        cancel = new JButton("Cancel");
        cancel.setActionCommand("cancel");

        add(save);
        add(edit);
        add(delete);
        add(cancel);
    }

    /**
     * Establecer el action listener a todos los botones
     * @param actionListener que ser� aplicado a los 4 botones.
     */
    public void setListeners(ActionListener actionListener) {
        save.addActionListener(actionListener);
        edit.addActionListener(actionListener);
        delete.addActionListener(actionListener);
        cancel.addActionListener(actionListener);
    }

    public void setEnabledState(boolean enabled) {
        setEnabledState(enabled, enabled, enabled, enabled);
    }

    /**
     * Modificar el atributo enabled de los botones
     * @param saveButton true para Enabled de otra manera false
     * @param editButton true para Enabled de otra manera false
     * @param deleteButton true para Enabled de otra manera false
     * @param cancelButton true para Enabled de otra manera false
     */
    public void setEnabledState(boolean saveButton, boolean editButton, boolean deleteButton, boolean cancelButton) {
        save.setEnabled(saveButton);
        edit.setEnabled(editButton);
        delete.setEnabled(deleteButton);
        cancel.setEnabled(cancelButton);
    }

    public void setSignUpMode() {
        save.setEnabled(true);
        edit.setEnabled(false);
        delete.setEnabled(false);
        cancel.setEnabled(true);
    }
}