package com.aurbano.musicManagement.componentsGUI.tables;

import com.aurbano.musicManagement.music.Album;
import com.aurbano.musicManagement.utils.HibernateUtils;
import org.hibernate.Query;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.util.List;

import static com.aurbano.musicManagement.app.Constants.ALBUM_HEADER;

/**
 * Created by adriu on 06/12/2015.
 */
public class AlbumTable extends JTable {

    private DefaultTableModel model;

    public AlbumTable() {
        model = new DefaultTableModel(ALBUM_HEADER, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
               return false;
            }
        };
        setModel(model);
    }

    public void setListener(ListSelectionListener listener) {
        getSelectionModel().addListSelectionListener(listener);
    }

    public int getSelectedItemID() {
        if (getSelectedRow() != -1)
            return (int) getValueAt(getSelectedRow(), 0);

        return -1;
    }

    public void clear() {
        model.setRowCount(0);
    }

    public void refresh(List<Album> albums) {
        model.setRowCount(0);

        for (Album album : albums) {
           Object[] row = new Object[] {
                   album.getId(),
                   album.getName(),
                   album.getRecordIndustry(),
                   album.getBandName(),
                   album.getUnitsSold(),
                   album.getPresentationDate()
           };

            if (!row[1].equals("noAlbum"))
                model.addRow(row);
        }
    }

    public Album getSelectedAlbum() {
        int selectedRow = getSelectedRow();

        if (selectedRow == -1)
            return null;

        Integer id = (Integer) getValueAt(selectedRow, 0);

        Query query = HibernateUtils.getCurrentSession().createQuery("SELECT a FROM Album a WHERE a.id = :id");
        query.setParameter("id", id);
        Album album = (Album) query.uniqueResult();

        return album;
    }
}
