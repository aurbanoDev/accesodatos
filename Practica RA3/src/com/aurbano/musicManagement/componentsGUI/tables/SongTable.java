package com.aurbano.musicManagement.componentsGUI.tables;

import com.aurbano.musicManagement.music.Song;
import com.aurbano.musicManagement.utils.HibernateUtils;
import org.hibernate.Query;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.util.List;

import static com.aurbano.musicManagement.app.Constants.SONG_HEADER;

/**
 * Created by adriu on 07/12/2015.
 */
public class SongTable extends JTable {

    private DefaultTableModel model;

    public SongTable() {
        model = new DefaultTableModel(SONG_HEADER, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        setModel(model);
    }

    public void setListener(ListSelectionListener listener) {
        getSelectionModel().addListSelectionListener(listener);
    }

    public int getSelectedItemID() {
        if (getSelectedRow() != -1)
            return (int) getValueAt(getSelectedRow(), 0);

        return -1;
    }

    public void clear() {
        model.setRowCount(0);
    }

    public void refresh(List<Song> songs) {
        model.setRowCount(0);

        for (Song song : songs) {
            Object[] row = new Object[] {
                    song.getId(),
                    song.getName(),
                    song.getMusicComposer(),
                    song.getLyricsComposer(),
                    song.getDuration(),
                    song.getReleaseDate()
            };

            if (!row[1].equals("noSong"))
                model.addRow(row);
        }
    }

    public Song getSelectedSong() {
        int selectedRow = getSelectedRow();

        if (selectedRow == -1)
            return null;

        Integer id = (Integer) getValueAt(selectedRow, 0);

        Query query = HibernateUtils.getCurrentSession().createQuery("SELECT a FROM Song a WHERE a.id = :id");
        query.setParameter("id", id);
        Song song = (Song) query.uniqueResult();

        return song;
    }
}
