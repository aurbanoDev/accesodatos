package com.aurbano.musicManagement.componentsGUI.tables;

import com.aurbano.musicManagement.music.VideoClip;
import com.aurbano.musicManagement.utils.HibernateUtils;
import org.hibernate.Query;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.util.List;

import static com.aurbano.musicManagement.app.Constants.CLIP_HEADER;

/**
 * Created by adriu on 07/12/2015.
 */
public class ClipTable extends JTable {

    private DefaultTableModel model;

    public ClipTable() {
        model = new DefaultTableModel(CLIP_HEADER, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        setModel(model);
    }

    public void setListener(ListSelectionListener listener) {
        getSelectionModel().addListSelectionListener(listener);
    }

    public int getSelectedItemID() {
        if (getSelectedRow() != -1)
            return (int) getValueAt(getSelectedRow(), 0);

        return -1;
    }

    public void clear() {
        model.setRowCount(0);
    }

    public void refresh(List<VideoClip> clips) {
        model.setRowCount(0);

        for (VideoClip clip : clips) {
            Object[] row = new Object[] {
                    clip.getId(),
                    clip.getCost(),
                    clip.getShootingDays(),
                    clip.getDirectorName(),
                    clip.getStudioName(),
                    clip.getRecordingDate()
            };

            if (!row[3].equals("noClip"))
                model.addRow(row);
        }
    }

    public VideoClip getSelectedClip() {
        int selectedRow = getSelectedRow();

        if (selectedRow == -1)
            return null;

        Integer id = (Integer) getValueAt(selectedRow, 0);

        Query query = HibernateUtils.getCurrentSession().createQuery("SELECT a FROM VideoClip a WHERE a.id = :id");
        query.setParameter("id", id);
        VideoClip clip = (VideoClip) query.uniqueResult();

        return clip;
    }

    public boolean isEmpty() {
        if (model.getRowCount() == 0)
            return true;

        return  false;
    }
}
