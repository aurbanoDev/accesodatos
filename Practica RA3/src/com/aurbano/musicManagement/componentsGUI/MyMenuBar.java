package com.aurbano.musicManagement.componentsGUI;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Menu bar con los JMenus y JMenuItems ya preparados.
 * @author Adrian Urbano
 */
public class MyMenuBar  extends JMenuBar {

    private JMenu file;
    private JMenu signUp;
    private JMenu admin;
    private JMenu xml;

    private JMenuItem userManagement;
    private JMenuItem deleteGroup;
    private JMenuItem newAlbum;
    private JMenuItem newSong;
    private JMenuItem newVideoClip;
    private JMenuItem autoRefresh;
    private JMenuItem closeSession;
    private JMenuItem importXML;
    private JMenuItem exportXML;
    private JMenuItem exit;

    public MyMenuBar() {
        file = new JMenu("File");
        signUp = new JMenu("New");
        admin = new JMenu("Admin");
        xml = new JMenu("XML");

        newAlbum = new JMenuItem("Album");
        newSong = new JMenuItem("Song");
        newVideoClip = new JMenuItem("VideoClip");
        userManagement = new JMenuItem("User Management");
        deleteGroup = new JMenuItem("Delete Group");
        exit = new JMenuItem("Exit");
        autoRefresh = new JMenuItem("Auto Refresh");
        closeSession = new JMenuItem("Close Session");
        importXML = new JMenuItem("Import XML");
        exportXML = new JMenuItem("Export XML");

        newAlbum.setActionCommand("newAlbum");
        newSong.setActionCommand("newSong");
        newVideoClip.setActionCommand("newVideoClip");
        userManagement.setActionCommand("userManagement");
        deleteGroup.setActionCommand("deleteGroup");
        exit.setActionCommand("exit");
        closeSession.setActionCommand("closeSession");
        importXML.setActionCommand("importXML");
        exportXML.setActionCommand("exportXML");
        autoRefresh.setActionCommand("refresh");

        signUp.add(newAlbum);
        signUp.add(newSong);
        signUp.add(newVideoClip);

        admin.add(userManagement);
        admin.setEnabled(false);

        xml.add(importXML);
        xml.add(exportXML);

        file.add(signUp);
        file.add(admin);
        file.add(autoRefresh);
        file.add(closeSession);
        file.add(exit);

        add(file);
        add(xml);
    }

    public void setListeners(ActionListener listener) {
        newAlbum.addActionListener(listener);
        newSong.addActionListener(listener);
        newVideoClip.addActionListener(listener);
        userManagement.addActionListener(listener);
        exit.addActionListener(listener);
        autoRefresh.addActionListener(listener);
        deleteGroup.addActionListener(listener);
        closeSession.addActionListener(listener);
        importXML.addActionListener(listener);
        exportXML.addActionListener(listener);
    }

    public void enableAdminMode(boolean enabled) {
        admin.setEnabled(enabled);
    }

}