package com.aurbano.musicManagement.componentsGUI.combos;

import com.aurbano.musicManagement.music.VideoClip;

/**
 * Created by adriu on 07/12/2015.
 */
public class ClipComboBox extends MyComboBox<VideoClip> {


    public void displaySelection(int id) {
        for (VideoClip clip : items) {
            if (id == clip.getId()) {
                setSelectedItem(clip);
                break;
            }
        }
    }
}
