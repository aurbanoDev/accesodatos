package com.aurbano.musicManagement.componentsGUI.combos;

import com.aurbano.musicManagement.music.Song;

/**
 * Created by adriu on 07/12/2015.
 */
public class SongComboBox extends MyComboBox<Song> {


    public void displaySelection(int id) {
        for (Song song : items) {
            if (id == song.getId()) {
                setSelectedItem(song);
                break;
            }
        }
    }
}
