package com.aurbano.musicManagement.componentsGUI.combos;

import com.aurbano.musicManagement.music.Album;

/**
 * Created by adriu on 07/12/2015.
 */
public class AlbumComboBox extends MyComboBox<Album> {



    public void displaySelection(int id) {
        for (Album album : items) {
            System.out.println("id = " + album.getId());
            if (id == album.getId()) {
                setSelectedItem(album);
                break;
            }
        }
    }
}
