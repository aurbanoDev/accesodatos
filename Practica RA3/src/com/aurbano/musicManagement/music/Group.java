package com.aurbano.musicManagement.music;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by adriu on 05/02/2016.
 */
@Entity
@Table(name = "groups")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name = "name")
    private String name;
    @Column(name = "creationDate")
    private Date creationDate;

    @ManyToMany(cascade = CascadeType.DETACH, mappedBy = "groups")
    private List<User> users;

    public Group() {
        this("", new Date());
    }

    public Group(String name, Date creationDate) {
        this.name = name;
        this.creationDate = creationDate;
        this.users = new ArrayList<>();
    }

    public Group(String groupName, Date creationDate, List<User> users) {
        this.name = groupName;
        this.creationDate = creationDate;
        this.users = users;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }
}
