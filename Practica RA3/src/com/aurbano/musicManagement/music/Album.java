package com.aurbano.musicManagement.music;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Clase que representa un Album de m�sica.
 * @author  Adrian Urbano
 */

@Entity
@Table(name = "Albums")
public class Album implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;
    @Column(name="recordIndustry")
    private String recordIndustry;
    @Column(name="bandName")
    private String bandName;
    @Column(name="unitsSold")
    private int unitsSold;
    @Column(name="presentationDate")
    private Date presentationDate;

    @OneToMany(mappedBy = "album", cascade = CascadeType.ALL)
    private List<Song> songs;


    public Album() {
        this("", "", "", 0, new Date());
    }

    public Album(String name, String recordIndustry, String bandName, int unitsSold, Date presentationDate) {
        this.name = name;
        this.recordIndustry = recordIndustry;
        this.bandName = bandName;
        this.unitsSold = unitsSold;
        this.presentationDate = presentationDate;
        this.songs = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getRecordIndustry() {
        return recordIndustry;
    }

    public String getBandName() {
        return bandName;
    }

    public Date getPresentationDate() {
        return presentationDate;
    }

    public int getUnitsSold() {
        return unitsSold;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRecordIndustry(String recordIndustry) {
        this.recordIndustry = recordIndustry;
    }

    public void setBandName(String bandName) {
        this.bandName = bandName;
    }

    public void setPresentationDate(Date presentationDate) {
        this.presentationDate = presentationDate;
    }

    public void setUnitsSold(int unitsSold) {
        this.unitsSold = unitsSold;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public boolean hasSongs(String name) {
        if (name.equalsIgnoreCase(""))
            return true;

        for (Song song : songs)
            if (song.getName().startsWith(name))
                return true;

        return false;
    }

    @Override
    public String toString() {
        return name;
    }
}
