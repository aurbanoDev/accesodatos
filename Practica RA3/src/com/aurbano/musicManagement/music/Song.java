package com.aurbano.musicManagement.music;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Clase que representa una Cancion.
 * @author  Adrian Urbano
 */

@Entity
@Table(name="songs")
public class Song implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @ManyToOne
    @JoinColumn(name="id_album")
    private Album album;

    @OneToOne(cascade=CascadeType.REMOVE, mappedBy="song")
    private VideoClip videoClip;

    @Column(name="name")
    private String name;
    @Column(name="musicComposer")
    private String musicComposer;
    @Column(name="lyricsComposer")
    private String lyricsComposer;
    @Column(name="duration")
    private float duration;
    @Column(name="releaseDate")
    private Date releaseDate;

    public Song() {
        this("", "", "", 0, new Date());
    }

    public Song(String name, String musicComposer, String lyricsComposer, float duration, Date releaseDate) {
        this.name = name;
        this.musicComposer = musicComposer;
        this.lyricsComposer = lyricsComposer;
        this.duration = duration;
        this.releaseDate = releaseDate;
    }

    public String getName() {
        return name;
    }

    public String getMusicComposer() {
        return musicComposer;
    }

    public String getLyricsComposer() {
        return lyricsComposer;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public float getDuration() {
        return duration;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMusicComposer(String musicComposer) {
        this.musicComposer = musicComposer;
    }

    public void setLyricsComposer(String lyricsComposer) {
        this.lyricsComposer = lyricsComposer;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public int getId() {
        return id;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public VideoClip getVideoClip() {
        return videoClip;
    }

    public void setVideoClip(VideoClip videoClip) {
        this.videoClip = videoClip;
    }

    @Override
    public String toString() {
        return name;
    }
}
