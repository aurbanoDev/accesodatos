package com.aurbano.musicManagement.music;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Clase que representa un VideoClip.
 * @author  Adrian Urbano
 */

@Entity
@Table(name = "videoclips")
public class VideoClip implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @OneToOne(cascade=CascadeType.DETACH)
    @JoinColumn(name="id_song")
    private Song song;

    @Column(name="cost")
    private float cost;
    @Column(name="shootingDays")
    private int shootingDays;
    @Column(name="directorName")
    private String directorName;
    @Column(name="studioName")
    private String studioName;
    @Column(name="recordingDate")
    private Date recordingDate;

    public VideoClip() {
        this(0, 0, "", "", new Date());
    }

    public VideoClip(float cost, int shootingDays, String directorName, String studioName, Date recordingDate) {
        this.cost = cost;
        this.shootingDays = shootingDays;
        this.directorName = directorName;
        this.studioName = studioName;
        this.recordingDate = recordingDate;
    }

    public Date getRecordingDate() {
        return recordingDate;
    }

    public String getStudioName() {
        return studioName;
    }

    public String getDirectorName() {
        return directorName;
    }

    public int getShootingDays() {
        return shootingDays;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public void setShootingDays(int shootingDays) {
        this.shootingDays = shootingDays;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public void setStudioName(String studioName) {
        this.studioName = studioName;
    }

    public void setRecordingDate(Date recordingDate) {
        this.recordingDate = recordingDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Song getSong() {
        return song;
    }

    public void setSong(Song song) {
        this.song = song;
    }

    @Override
    public String toString() {
        return  directorName;
    }
}
