package com.aurbano.musicManagement.dataAccessObjects;

import com.aurbano.musicManagement.music.Song;
import com.aurbano.musicManagement.utils.HibernateUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adriu on 03/12/2015.
 */
public class DataBaseSongDAO implements GenericDAO<Song> {


    @Override
    public void add(Song song) {
        Session session = HibernateUtils.getCurrentSession();
        session.beginTransaction();
        session.save(song);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(Song song) {
        Session sesion = HibernateUtils.getCurrentSession();
        sesion.beginTransaction();
        sesion.update(song);
        sesion.getTransaction().commit();
        sesion.close();
    }

    @Override
    public void delete(Song item) {
        Session session = HibernateUtils.getCurrentSession();
        session.beginTransaction();
        session.delete(item);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public ArrayList<Song> getList() {
        Query query = HibernateUtils.getCurrentSession().createQuery("FROM Song");
        List<Song> songs = query.list();

        return (ArrayList<Song>) songs;
    }

   public List<Song> getUnbindedSongs() {
       ArrayList<Song> songs = new ArrayList<>();

       for (Song song : getList()) {
           if (song.getVideoClip() == null)
               songs.add(song);
       }

       return songs;
   }
}
