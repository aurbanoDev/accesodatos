package com.aurbano.musicManagement.dataAccessObjects;

import com.aurbano.musicManagement.music.Album;
import com.aurbano.musicManagement.utils.HibernateUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by adriu on 03/12/2015.
 */
public class DataBaseAlbumDAO implements GenericDAO<Album> {

    @Override
    public void add(Album album) {
        Session session = HibernateUtils.getCurrentSession();
        session.beginTransaction();
        session.save(album);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(Album album) {
        Session sesion = HibernateUtils.getCurrentSession();
        sesion.beginTransaction();
        sesion.update(album);
        sesion.getTransaction().commit();
        sesion.close();
    }


    @Override
    public void delete(Album item) {
        Session session = HibernateUtils.getCurrentSession();
        session.beginTransaction();
        session.delete(item);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public List<Album> getList() {
        Query query = HibernateUtils.getCurrentSession().createQuery("FROM Album");
        List<Album> albums = query.list();

        return albums;
    }
}
