package com.aurbano.musicManagement.dataAccessObjects;

import com.aurbano.musicManagement.utils.DataBase;

/**
 * Created by adriu on 03/12/2015.
 */
public class DataBaseDAOfactory {

    public DataBaseSongDAO getSongDAO() {
        return new DataBaseSongDAO();
    }

    public DataBaseAlbumDAO getAlbumDAO() {
        return new DataBaseAlbumDAO();
    }

    public DataBaseClipDAO getClipDAO() {
        return new DataBaseClipDAO();
    }
}
