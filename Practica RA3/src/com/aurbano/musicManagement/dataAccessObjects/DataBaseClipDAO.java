package com.aurbano.musicManagement.dataAccessObjects;

import com.aurbano.musicManagement.music.VideoClip;
import com.aurbano.musicManagement.utils.HibernateUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adriu on 03/12/2015.
 */
public class DataBaseClipDAO implements GenericDAO<VideoClip> {

    @Override
    public void add(VideoClip clip) {
        Session session = HibernateUtils.getCurrentSession();
        session.beginTransaction();
        session.save(clip);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public void update(VideoClip clip) {
        Session sesion = HibernateUtils.getCurrentSession();
        sesion.beginTransaction();
        sesion.update(clip);
        sesion.getTransaction().commit();
        sesion.close();
    }

    @Override
    public void delete(VideoClip item) {
        Session session = HibernateUtils.getCurrentSession();
        session.beginTransaction();
        session.delete(item);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public List<VideoClip> getList() {
        Query query = HibernateUtils.getCurrentSession().createQuery("FROM VideoClip");
        List<VideoClip> videoclips = query.list();

        return videoclips;
    }

    public ArrayList<VideoClip> getUnbindedClips() {
        ArrayList<VideoClip> clips = new ArrayList<>();

        for (VideoClip clip : getList()) {
            if (clip.getSong() == null)
                clips.add(clip);
        }

        return clips;
    }
}
