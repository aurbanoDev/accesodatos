package com.aurbano.musicManagement.utils;

import com.aurbano.musicManagement.music.Album;
import com.aurbano.musicManagement.music.VideoClip;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Clase de utilidades.
 * @author Adrian Urbano
 */
public class Utils {

    public static boolean isNumber(String text) {
        Pattern notNumber = Pattern.compile("[^0-9]+(\\.[^0-9][^0-9]?)?");
        Matcher matcher = notNumber.matcher(text);

        return (matcher.matches()) ?  false : true ;
    }

    public static void showDialog(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    public static void showErrorDialog(String message, String title) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Crea un documento XML con la lista de VideoClips proporcionada.
     * @param list que sera exportada como documento XML
     */
    public static void exportClipsAsXML(List<VideoClip> list) {

        if (list.isEmpty()) {
            showErrorDialog("The list is empty", "alert");
            return;
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        Document document = null;
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            document = dom.createDocument(null, "xml", null);

            Element root = document.createElement("videoclips");
            document.getDocumentElement().appendChild(root);

            Element clipNode = null, dataNode = null;
            Text text = null;

            for (VideoClip clip : list) {
                clipNode = document.createElement("videoclip");
                root.appendChild(clipNode);

                dataNode = document.createElement("directorname");
                clipNode.appendChild(dataNode);
                text = document.createTextNode(clip.getDirectorName());
                dataNode.appendChild(text);

                dataNode = document.createElement("studioname");
                clipNode.appendChild(dataNode);
                text = document.createTextNode(clip.getStudioName());
                dataNode.appendChild(text);

                dataNode = document.createElement("shootingdays");
                clipNode.appendChild(dataNode);
                text = document.createTextNode(String.valueOf(clip.getShootingDays()));
                dataNode.appendChild(text);

                dataNode = document.createElement("cost");
                clipNode.appendChild(dataNode);
                text = document.createTextNode(String.valueOf(clip.getCost()));
                dataNode.appendChild(text);

                dataNode = document.createElement("recordingdate");
                clipNode.appendChild(dataNode);
                text = document.createTextNode(String.valueOf(clip.getRecordingDate()));
                dataNode.appendChild(text);

                if (clip.getSong()!= null) {

                    Element songRoot = document.createElement("song");
                    clipNode.appendChild(songRoot);

                    Element songNode = document.createElement("name");
                    songRoot.appendChild(songNode);

                    text = document.createTextNode(clip.getSong().getName());
                    songRoot.appendChild(text);

                    songNode = document.createElement("musiccomposer");
                    songRoot.appendChild(songNode);
                    text = document.createTextNode(clip.getSong().getMusicComposer());
                    songRoot.appendChild(text);

                    songNode = document.createElement("lyricscomposer");
                    songRoot.appendChild(songNode);
                    text = document.createTextNode(clip.getSong().getLyricsComposer());
                    songRoot.appendChild(text);

                    songNode = document.createElement("duration");
                    songRoot.appendChild(songNode);
                    text = document.createTextNode(String.valueOf(clip.getSong().getDuration()));
                    songRoot.appendChild(text);

                    songNode = document.createElement("releasedate");
                    songRoot.appendChild(songNode);


                    text = document.createTextNode(String.valueOf(clip.getSong().getReleaseDate()));
                    songRoot.appendChild(text);
                }

                Source source = new DOMSource(document);
                Result result = new StreamResult(new File("clips.xml"));

                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                transformer.transform(source, result);
            }

        } catch (ParserConfigurationException pex) {

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    public static ArrayList<Album> getAlbumFromXml(String path) {

        ArrayList<Album> listado = new ArrayList<>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            documento = builder.parse(new File(path));

            NodeList albums = documento.getElementsByTagName("album");
            for (int i = 0; i < albums.getLength(); i++) {
                Node album = albums.item(i);
                Element elemento = (Element) album;

                Album albumcico = new Album();

                albumcico.setName(elemento.getElementsByTagName("name").item(0).
                        getChildNodes().item(0).getNodeValue());

                albumcico.setRecordIndustry(elemento.getElementsByTagName("recordIndustry").item(0).
                        getChildNodes().item(0).getNodeValue());

                albumcico.setBandName(elemento.getElementsByTagName("bandName").item(0).
                        getChildNodes().item(0).getNodeValue());

                albumcico.setUnitsSold(Integer.parseInt(elemento.getElementsByTagName("unitsSold").item(0).
                        getChildNodes().item(0).getNodeValue()));

                String fecha = elemento.getElementsByTagName("presentationDate").item(0).
                        getChildNodes().item(0).getNodeValue();

                albumcico.setPresentationDate(getSqlDateFromString(fecha));

                listado.add(albumcico);
            }

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (SAXException saxe) {
            saxe.printStackTrace();
        }

        return listado;
    }

    public static java.sql.Date getDateSql(java.util.Date fecha) {
        return new java.sql.Date(fecha.getTime());
    }

    public static java.sql.Date getSqlDateFromString(String fecha) {

        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");

        try {
            return getDateSql(formatoDelTexto.parse(fecha));

        } catch (ParseException ex) {
            ex.printStackTrace();
        }

        return null;
    }



}






