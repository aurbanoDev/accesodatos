package com.aurbano.musicManagement.utils;

import com.aurbano.musicManagement.music.Group;
import com.aurbano.musicManagement.music.User;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by adriu on 03/12/2015.
 */
public class DataBase {

    public void startConnection() {
        HibernateUtils.buildSessionFactory();
        HibernateUtils.openSession();
    }

    public void disconnect() {
        HibernateUtils.closeSessionFactory();
    }

    public boolean checkLogin(String login, String password) {
        Session session = HibernateUtils.getCurrentSession();
        String hql = "SELECT a FROM User a WHERE a.username = :username AND a.password = :password";

        Query query = session.createQuery(hql);
        query.setParameter("username", login);
        query.setParameter("password", password);

        User user = (User) query.uniqueResult();
        if (user == null)
            return false;

        return true;
    }

    public String getUserRole(String login) {
        Session session = HibernateUtils.getCurrentSession();

        Query query = session.createQuery("SELECT a FROM User a WHERE a.username = :username");
        query.setParameter("username", login);

        User user = (User) query.uniqueResult();

       return user.getRol();
    }


    public void saveUser(User user) {
        Session session = HibernateUtils.getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(user);
        session.getTransaction().commit();
        session.close();
    }

    public boolean userExists(String username) {
        Session session = HibernateUtils.getCurrentSession();
        Query query = session.createQuery("SELECT a FROM User a WHERE a.username = :username");
        query.setParameter("username", username);

        User user = (User) query.uniqueResult();

       return (user == null) ? false : true;
    }

    public List<User> getUsers() {
        Session session = HibernateUtils.getCurrentSession();
        Query query = session.createQuery("FROM User");

        return query.list();
    }

    public List<Group> getGroups() {
        Session session = HibernateUtils.getCurrentSession();
        Query query = session.createQuery("FROM Group");

        return query.list();
    }

    public void deleteUser(User user) {
        Session session = HibernateUtils.getCurrentSession();
        session.beginTransaction();
        session.delete(user);
        session.getTransaction().commit();
        session.close();
    }

    public void  saveGroup(Group group) {
        Session session = HibernateUtils.getCurrentSession();
        session.beginTransaction();
        session.saveOrUpdate(group);
        session.getTransaction().commit();
        session.close();
    }

    public void deleteGroup(Group group) {
        Session session = HibernateUtils.getCurrentSession();
        session.beginTransaction();
        session.delete(group);
        session.getTransaction().commit();
        session.close();
    }
}

