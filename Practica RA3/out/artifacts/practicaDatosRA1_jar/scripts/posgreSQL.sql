
CREATE TYPE ROL_TYPE AS ENUM ('admin', 'user');

CREATE TABLE USERS(
	id serial PRIMARY KEY,
	username text UNIQUE NOT NULL,
	password text NOT NULL,
	rol ROL_TYPE
);

CREATE TABLE ALBUMS(
	id serial PRIMARY KEY,
	name text,
	recordIndustry text,
	bandName text,
	unitsSold integer DEFAULT 0,
	presentationDate DATE
);

CREATE TABLE SONGS(
	id serial PRIMARY KEY,
	name text,
	musicComposer text,
	lyricsComposer text,
	duration real default 0,
	releaseDate DATE,
	id_album integer,
	id_videoclip integer
);

CREATE TABLE VIDEOCLIPS(
	id serial PRIMARY KEY,
	cost real default 0,
	shootingDays integer default 0,
	directorName text,
	studioName text,
	recordigDate DATE,
	id_song integer
);

INSERT INTO USERS(username, password, rol) values('admin', md5('admin') , 'admin');
INSERT INTO USERS(username, password, rol) values('user', md5('user') , 'user');
INSERT INTO ALBUMS(name, recordIndustry, bandName, unitsSold, presentationDate) values('noAlbum','','', 0 , null);
INSERT INTO SONGS(name, musicComposer, lyricsComposer, duration, releaseDate, id_videoclip, id_album) values('noSong', '', '', 0, null, 1, 1);
INSERT INTO VIDEOCLIPS(cost, shootingDays, directorName, studioName, recordigDate, id_song) values(0, 0, 'noClip', '', null, 1);
